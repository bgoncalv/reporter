"""Main reporter definition."""
from argparse import ArgumentParser
from functools import partial
import sys

from cki_lib import metrics
from cki_lib import misc
import sentry_sdk

from . import settings
from .data import CheckoutData
from .emailer import render_template
from .emailer import send_emails


class ReporterException(Exception):
    """An exception in reporting."""


def print_report(checkout_data):
    """Render the checkout's report and print it."""
    print(render_template(checkout_data))


def process_message(body=None, func=None, **_):
    """Process the webhook message."""
    settings.LOGGER.info('processing message (body): %s', body)
    if body['status'] != 'ready_to_report':
        settings.LOGGER.info('Skipping status: "%s"', body['status'])
        return
    if misc.get_nested_key(body, 'object/misc/retrigger', False):
        settings.LOGGER.info('Skipping retriggered')
        return
    if body.get('object_type') == 'checkout':
        # If this checkout is related to a merge request, we don't need to report anything
        if misc.get_nested_key(body, 'object/misc/related_merge_request'):
            return
        checkout_data = CheckoutData(body['id'])
        func(checkout_data)
    else:
        error = 'Invalid "ready_to_report" message received'
        settings.LOGGER.error(error)
        raise ReporterException(error)


def create_parser():
    """Create argument parser."""
    parser = ArgumentParser('Create reports for provided pipelines')
    parser.add_argument('-t', '--template', type=str,
                        metavar='TEMPLATE_NAME',
                        choices=list(settings.TEMPLATES.keys()),
                        help='Template name to be used. When email reporting, '
                        'will be used as a default template for recipients that '
                        'don\'t have a template specified.')
    parser.add_argument('--email', action='store_true',
                        help='Send an email instead of printing out the report.')
    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('-l', '--listen', action='store_true',
                       help='Start polling for rabbitmq messages and create ' +
                       'reports for ready_to_report checkouts.' +
                       ' Defaults to this if no other option was selected.')
    group.add_argument('-c', '--checkout_id', type=str,
                       help='Specify the checkout id of a checkout ' +
                       'you want to create a single report for.')
    return parser


def main():
    """Set up and start consuming messages."""
    misc.sentry_init(sentry_sdk)
    parser = create_parser()
    args = parser.parse_args()

    if args.template is not None:
        settings.SELECTED_TEMPLATE = args.template
    if args.email:
        settings.EMAILING_ENABLED = True

    processing_function = send_emails if settings.EMAILING_ENABLED else print_report

    if args.checkout_id:
        checkout_data = CheckoutData(args.checkout_id)
        processing_function(checkout_data)
        # exit code based on report result for local runs
        if not misc.is_production():
            sys.exit(0 if checkout_data.result else 1)  # return 1 on failure
    else:
        # Default to rabbitmq consuming
        print('Now consuming Rabbitmq messages')
        metrics.prometheus_init()
        settings.QUEUE.consume_messages(
            settings.REPORTER_EXCHANGE,
            ['#'],
            partial(process_message, func=processing_function),
            queue_name=settings.REPORTER_QUEUE
        )


if __name__ == '__main__':
    main()
