from unittest import TestCase

import responses

from reporter.data import CheckoutData

BUILDS = [
        {
            'id': 0,
            'misc': {'iid': 0},
        },
        {
            'id': 1,
            'misc': {'iid': 1},
        },
    ]

DATA = {
        0: {
            'id': 0,
            'misc': {'iid': 0},
        },
    }


class TestRequests(TestCase):
    """Tests for the API requests done by reporter."""

    @responses.activate
    def test_build_list(self):
        """Test the listing of builds."""
        responses.add(
            responses.GET,
            'http://localhost/api/1/kcidb/checkouts/0',
            json=DATA[0]
        )
        responses.add(
            responses.GET,
            'http://localhost/api/1/kcidb/checkouts/0/builds',
            json={
                'results': BUILDS,
                'count': 2,
            }
        )
        responses.add(
            responses.GET,
            'http://localhost/api/1/kcidb/builds/0/issues/occurrences',
            json={
                'results': [],
                'count': 0,
            }
        )
        responses.add(
            responses.GET,
            'http://localhost/api/1/kcidb/builds/1/issues/occurrences',
            json={
                'results': [],
                'count': 0,
            }
        )

        checkout = CheckoutData(0)
        builds = checkout.build_data.builds
        self.assertEqual(2, len(builds))
        self.assertEqual(
            [BUILDS[0]['id'], BUILDS[1]['id']],
            [build.id for build in builds]
        )

        # Make sure the builds iterator is reusable
        self.assertEqual(
            [BUILDS[0]['id'], BUILDS[1]['id']],
            [build.id for build in builds]
        )
